from django.test import Client, TestCase
import json


class CalculateViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = 'http://localhost:8000/calculate/'

    def test_correct_request_with_div_str_numbers(self):
        data = {
            'first_operand': '4.45',
            'second_operand': '2',
            'operation': '/',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'result': 2.225})

    def test_correct_request_with_sum_real_numbers(self):
        data = {
            'first_operand': 6,
            'second_operand': 4.5,
            'operation': '+',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'result': 10.5})

    def test_correct_request_with_mul_numbers(self):
        data = {
            'first_operand': '6',
            'second_operand': 3,
            'operation': '*',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'result': 18})

    def test_correct_request_with_sub_numbers(self):
        data = {
            'first_operand': 6,
            'second_operand': '1',
            'operation': '-',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'result': 5})

    def test_incorrect_operator(self):
        data = {
            'first_operand': 6,
            'second_operand': "4.5sdf",
            'operation': '+',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'error': 'Operands should be numbers!'})

    def test_incorrect_operation(self):
        data = {
            'first_operand': 6,
            'second_operand': "4.5",
            'operation': '&',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'error': 'Unknown operation!'})

    def test_incorrect_json_data_in_request(self):
        data = {
            'frst_op': 6,
            'sec_op': 4,
            'op': '+',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'error': "Can't extract needed data!"})

    def test_divizion_by_zero(self):
        data = {
            'first_operand': 6,
            'second_operand': '0',
            'operation': '/',
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'error': 'DivisionByZero!'})

    def test_incorrect_request_method(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'error': 'Invalid method!'})
        