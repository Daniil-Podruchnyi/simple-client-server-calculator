import json
from django.http import HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Декоратор используется для межсайтовых запросов, отключает для них потверждение по токену
@csrf_exempt
def calculate(request: HttpRequest) -> JsonResponse:
    # Если запрос с методом POST, то обрабатываем его
    if request.method == 'POST':
        # Извлекаем данные в json формате
        data = json.loads(request.body.decode('utf-8'))

        try:
            # Извлекаем данные в переменные, заранее преобразуя операнды в числа
            first_operand = float(data['first_operand'])
            second_operand = float(data['second_operand'])
            operation = data['operation']
        except ValueError:
            # Если возникло исключение, значит операнд был не числом, выдаем ошибку
            return JsonResponse({'error': 'Операнды должны быть числами!'})
        except KeyError:
            # Если возникло это исключение, значит был передан не корректный json в запросе
            return JsonResponse({'error': "Не корректный запрос (BadJson)!"})

        # В зависимости от оператора вычисляем результат
        if operation == '+':
            result = first_operand + second_operand
        elif operation == '-':
            result = first_operand - second_operand
        elif operation == '*':
            result = first_operand * second_operand
        elif operation == '/':
            # Обрабатываем исключение, связанное с делением на ноль
            try:
                result = first_operand / second_operand
            except ZeroDivisionError:
                return JsonResponse({'error': 'Деление на ноль!'})
        else:
            # Обрабатываем случай передачи неизвестного оператора
            return JsonResponse({'error': 'Неизвестный оператор!'})
        
        # Если все хорошо, возвращаем результат
        return JsonResponse({'result': result})
    
    else:
        # Обрабатываем случай не корректного выбора метода запроса
        return JsonResponse({'error': 'Не корректный метод запроса!'})
