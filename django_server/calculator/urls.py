from django.urls import path
from calculator import views

urlpatterns = [
    path('calculate/', views.calculate),
]