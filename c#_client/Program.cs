﻿using System.Net;
using System.Text;
using System.Text.Json;

namespace Calculation
{
    // Создаем класс для преобразования данных в json формат
    public class Calculation
    {
        public string first_operand { get; set; }
        public string second_operand { get; set; }
        public string operation{ get; set; }
    }

    public class Result
    {
        public float result { get; set;}
    }

    public class Error
    {
        public string error { get; set; }
    }

    public class Program
    {

        static void Main()
        {   // Делаем интерактивное меню
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }
        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Доступные опции:");
            Console.WriteLine("1) Посчитать");
            Console.WriteLine("2) Выйти");
            Console.Write("\r\nВыберите опцию: ");
 
            switch (Console.ReadLine())
            {
            case "1":
                Calculate(); // При выборе первой опции вызываем функцию для запроса пользовательских данных
                return true;
            case "2":
                return false;
            default:
                return true;
            }
        }

        private static void Calculate()
        {
            Console.Clear();
            Console.WriteLine("///Простой калькулятор\\\\\\");
            Console.WriteLine("---Формат дробных чисел: 3.456 (Через точку)---");
            Console.WriteLine("---Доступные операции: (+, -, *, /) ---");

            var calculationData = CaptureInput(); // Получаем наш подготовленный обьект для преобразования в json
            var preparedData = MakeJson(calculationData); // Преобразуем в json, получаем строку
            var response = MakeRequestToApi(preparedData); // Отправляем запрос к нашему Api для вычисления результата
 
            DisplayResult(response); // Отображаем результат
        }
    
        static Calculation CaptureInput()
        {   
            // Создаем класс на основе которого будет создана json строка
            var calculationData = new Calculation();

            Console.Write("Введите первый операнд: ");
            calculationData.first_operand = Console.ReadLine();
            Console.Write("Введите второй операнд: ");
            calculationData.second_operand = Console.ReadLine();
            Console.Write("Введите операцию(+,-,*,/): ");
            calculationData.operation = Console.ReadLine();

            return calculationData;
        }

        static string MakeJson(Calculation calculationData) {
            string jsonString = JsonSerializer.Serialize(calculationData);
            return jsonString;
        }

        static string MakeRequestToApi(string jsonData) {
            string url = "http://localhost:8000/calculate/";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url); // создаем объект запроса
            httpWebRequest.ContentType = "application/json"; // указываем тип содержимого в запросе
            httpWebRequest.Method = "POST"; // указываем метод запроса - POST

            // создаем байтовый массив из строки с параметрами в формате JSON
            byte[] byteArray = Encoding.UTF8.GetBytes(jsonData);
            httpWebRequest.ContentLength = byteArray.Length; // указываем длину тела запроса в байтах

            using (var dataStream = httpWebRequest.GetRequestStream())
            {
                // отправляем данные в запросе
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            // получаем ответ от сервера
            using (var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                // выводим данные из ответа
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    // Проверяем возникла ошибка или нет, в зависимости от этого десериализуем json
                    if (result.Contains("error")) {
                        Error response = JsonSerializer.Deserialize<Error>(result);
                        return response.error;
                    } else {
                        Result response = JsonSerializer.Deserialize<Result>(result);
                        return response.result.ToString();
                    }
                }
            }
        }

        private static void DisplayResult(string message)
        {   
            Console.WriteLine($"\r\nВаш результат: {message}");
            Console.Write("\r\nНажмите Enter чтобы вернуться в меню");
            Console.ReadLine();
        }
    }
}
